get '/simulator' do
  slim :'/simulator/index'
end

post '/simulator/data/real-demand' do
  Tables::Generation.between_date(
    date_param(params[:dates][:from]),
    date_param(params[:dates][:to])
  ).to_json
end
