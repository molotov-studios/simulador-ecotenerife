require_relative 'energy'

module Simulator
  class ManageableEnergy < Simulator::Energy
    def self.id_name
      'manageable'
    end

    def self.display_name
      I18n.t 'simulator.energy.manageable.name'
    end

    def self.overall_name
      I18n.t 'simulator.energy.manageable.overall'
    end
  end
end
