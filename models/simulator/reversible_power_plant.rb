require_relative 'energy'

module Simulator
  class ReversiblePowerPlant < Energy
    def self.id_name
      'reversible'
    end

    def self.display_name
      I18n.t 'simulator.energy.reversible.name'
    end
  end
end
