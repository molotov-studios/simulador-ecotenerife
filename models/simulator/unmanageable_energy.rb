require_relative 'energy'

module Simulator
  class UnmanageableEnergy < Simulator::Energy
    def self.id_name
      'unmanageable'
    end

    def self.display_name
      I18n.t 'simulator.energy.unmanageable.name'
    end

    def self.overall_name
      I18n.t 'simulator.energy.unmanageable.overall'
    end
  end
end
