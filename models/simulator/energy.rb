module Simulator
  class Energy
    include DataMapper::Resource

    property :id, Serial
    property :type, Discriminator

    property :name, String, unique: true
    property :min_watt, Integer
    property :max_watt, Integer

    def default_enabled
      true
    end

    def default_value
      min_watt + (max_watt - min_watt) / 2
    end

    def display_name
      I18n.t "simulator.energy.types.#{name}.name"
    end

    def unit
      self.class.unit
    end

    def self.unit
      I18n.t 'simulator.energy.unit'
      "MW"
    end
  end
end
