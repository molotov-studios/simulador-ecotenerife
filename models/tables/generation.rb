module Tables
  class Generation
    include DataMapper::Resource

    property :id, Serial
    property :date, DateTime, required: true, unique: true

    property :diesel_engines, Float, required: true
    property :electrical, Float, required: true
    property :gas_turbine, Float, required: true
    property :wind, Float, required: true
    property :combined_cycle, Float, required: true
    property :steam_turbine, Float, required: true
    property :solar_photovoltaic, Float, required: true
    property :hydraulics, Float, required: true

    property :real_demand, Float, required: true

    DATA_ATTRS = %i(date diesel_engines electrical gas_turbine wind combined_cycle steam_turbine
                    solar_photovoltaic hydraulics real_demand)

    def self.between_date(first, last)
      all(order: :date.asc, :date.gte => first, :date.lte => last)
    end
  end
end
