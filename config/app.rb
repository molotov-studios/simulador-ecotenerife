configure do
  set :sockets, []
  set :sessions, true

  set :application_color, '#982a2a'
end

configure :development do
  require 'better_errors'

  use BetterErrors::Middleware
  BetterErrors.application_root = __dir__
end

Dir["#{settings.root}/managers/**/*.rb"].each { |f| require f }
