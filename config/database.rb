configure do
  DataMapper::setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{settings.root}/database.db")
  Dir["#{settings.root}/models/**/*.rb"].each { |file| require file }
  DataMapper.finalize
  DataMapper.auto_upgrade!
end
