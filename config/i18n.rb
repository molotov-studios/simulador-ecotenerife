require 'i18n'
require 'i18n/backend/fallbacks'
require 'i18n-js'

configure do
  I18n::Backend::Simple.send :include, I18n::Backend::Fallbacks
  I18n.load_path += Dir[File.join settings.root, 'config', 'locales', '*.yml']
  I18n.backend.load_translations

  I18n.available_locales = [:es]
  I18n.default_locale = I18n.available_locales.first

  I18n::JS.export
end
