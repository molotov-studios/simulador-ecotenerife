source 'https://rubygems.org'

ruby '2.2.4'

# General
gem 'rake'

# Sinatra
gem 'sinatra', '~> 1.4.7'
gem 'sinatra-websocket'
gem 'sinatra-assetpack', '~> 0.3.5'
gem 'sinatra-contrib', require: false
gem 'sinatra-resources'
gem 'thin'
gem 'activesupport'

# Database
gem 'data_mapper'
gem 'dm-pager'
gem 'bowtie'

# Front-end
gem 'slim'
gem 'less'
gem 'therubyracer'
gem 'json'
gem 'uglifier'
gem 'coffee-script'

# Internationalization/Localization
gem 'i18n'
gem 'i18n-js', '>= 3.0.0.rc11'

# Development
group 'development' do
  # Database adapter (SQLite)
  gem 'dm-sqlite-adapter'

  # Debugging tools
  gem 'pry'
  gem 'pry-nav'
  gem 'awesome_print'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'racksh'
end

# Production
group 'production' do
  # Database adapter (PostgreSQL)
  gem 'dm-postgres-adapter'
end
