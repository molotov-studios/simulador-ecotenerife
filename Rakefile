require './app'
require 'i18n-js'

namespace :db do
  desc 'Initialize the database'
  task :seed do
    DataMapper::Model.raise_on_save_failure = false

    # Manageable Energy
    Simulator::ManageableEnergy.create name: 'geothermal', min_watt: 0, max_watt: 200
    Simulator::ManageableEnergy.create name: 'solarthermal', min_watt: 0, max_watt: 400
    Simulator::ManageableEnergy.create name: 'biomass', min_watt: 0, max_watt: 200
    Simulator::ManageableEnergy.create name: 'biofuel', min_watt: 0, max_watt: 100

    Simulator::UnmanageableEnergy.create name: 'wind', min_watt: 0, max_watt: 400
    Simulator::UnmanageableEnergy.create name: 'photovoltaic', min_watt: 0, max_watt: 300

    Simulator::ReversiblePowerPlant.create name: 'pumping', min_watt: 0, max_watt: 300

    # Generation Table
    data = CSV.read(
      'data/EcoTenerife - TestData.csv',
      :headers => true, :header_converters => :symbol, :converters => [:float]
    )

    attrs = Tables::Generation::DATA_ATTRS
    data.each do |row|
      generation_data = row.fields.each.with_index.each_with_object({}) do |(f, i), hash|
        hash[attrs[i]] = f
      end
      generation_data[:date] = DateTime.strptime(generation_data[:date], "%m/%d/%Y %H:%M:%S")
      Tables::Generation.create generation_data
    end
  end
end

namespace :i18n do
  namespace :js do
    desc 'Export translations to JS file(s)'
    task :export do
      I18n::JS.export
    end
  end
end
