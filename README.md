# Simulador EcoTenerife

## Resumen

Simulador de energías renovables para la isla de Tenerife (Canarias, España). Versión en vivo:

- http://simulador-ecotenerife.herokuapp.com/

## Instalación

Clonar el repositorio, e instalar las dependencias con el comando

````sh
bundle install
````

La base de datos puede ser poblada con

```sh
bundle exec rake db:seed
```

El servidor puede iniciarse con

````sh
bundle exec rackup
````

## Autores

Este proyecto se ha realizado por las siguientes personas:

| Avatar | Nombre | Nickname | Correo |
| ------- | ------------- | --------- | ------------------ |
| ![](http://www.gravatar.com/avatar/2ae6d81e0605177ba9e17b19f54e6b6c.jpg?s=64)  | Daniel Herzog | Wikiti | [wikiti.doghound@gmail.com](mailto:wikiti.doghound@gmail.com) |
| ...  | ... | ... | ... |
