# Setup bundler (dependencies)
require 'bundler'
Bundler.require

# Load ActiveSupport content
require 'active_support'
require 'active_support/all'

require 'time'

# Set load path
set :root, File.dirname(__FILE__)
$LOAD_PATH.unshift settings.root unless $LOAD_PATH.include? settings.root

# Development
%w(pry sinatra/reloader).each { |f| require f } if development?

# Initialize
Dir["#{settings.root}/config/**/*.rb"].each { |f| require f }

configure :development do
  %w(config models routes managers).each { |folder| also_reload "#{settings.root}/#{folder}/**/*.rb" }
end
