require './app'

map '/admin' do
  BOWTIE_AUTH = { user: ENV['ADMIN_USER'] || 'admin', pass: ENV['ADMIN_PASSWORD'] || 'admin' }
  run Bowtie::Admin
end

map '/' do
  run Sinatra::Application
end
