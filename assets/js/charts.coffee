# TODO: Show a message before the chart is loaded, with some text:
# "please, select a configuration to show the graph"

DemandChart = do ->
  # Singleton object
  obj = {}

  # Private data
  draw_running = false

  request_data = null # Data requested to the server
  chart_data = null # Processed data

  current_data = null # Current chart representation
  current_index = null # Last element found
  current_speed = 0.0001 # 1 # Speed factor
  interval = null # Interval handler

  manageable_properties = ['geothermal', 'solarthermal', 'biofuel', 'biomass']

  # Private functions
  showSpinner = ->
    console.debug "TODO: Show a cute spinner"

  hideSpinner = ->
    console.debug "TODO: Hide the cute spinner"

  powerForInstant = (instant) ->
    manageable_values = Configuration.config().properties
    demand = instant.real_demand
    output = {}

    # TODO: Do something with non manageable energy.
    # This method will postulate that this value will be 0.
    output.demand = demand

    # Loop until demand is filled
    for property in manageable_properties
      if Configuration.isPropertyEnabled(property)
        value = manageable_values[property].value
        output[property] = Utils.min(demand, value)
        demand -= output[property]

    # TODO: Add some real messages to this error
    console.warn "Not enough power" if demand > 0

    output

  generateChartData = () ->
    chart_data = []

    # Add header (titles)
    header = [I18n.t('simulator.chart.date'), I18n.t('simulator.chart.demand')]
    for property in manageable_properties
      if Configuration.isPropertyEnabled(property)
        header.push(I18n.t("simulator.energy.types.#{property}.name"))
    chart_data.push(header)

    # Add values for each instant
    request_data.forEach (instant) ->
      power = powerForInstant(instant)

      row = [new Date(instant.date), power.demand]
      for property in manageable_properties
        if Configuration.isPropertyEnabled(property)
          row.push(power[property])
      chart_data.push(row)

    chart_data

  currentIntervalTime = ->
    SpeedControls.currentInterval()

  stopDrawInterval = ->
    clearInterval(interval) if(interval)
    return

  currentMinDate = ->
    new Date(request_data[0].date)

  currentMaxDate = ->
    new Date(request_data[request_data.length - 1].date)

  drawNextChart = ->
    current_index += 1
    if current_index < chart_data.length
      current_data = chart_data[0..current_index]
      drawChart()
    else
      # TODO: Stop simulation
      console.debug 'Simulation has ended'
      stopDrawInterval()
    return

  drawChart = ->
    data = google.visualization.arrayToDataTable(current_data)

    options =
      title: I18n.t('simulator.chart.title')
      isStacked: true
      seriesType: 'area'
      series:
        0:
          type: 'line'
      interpolateNulls: true
      hAxis:
        title: 'Date'
        format: 'dd/MM/yyyy'
        titleTextStyle: color: '#333'
        minValue: currentMinDate()
        maxValue: currentMaxDate()
      vAxis:
        minValue: 0
    chart = new google.visualization.ComboChart($('#chart-container')[0])
    chart.draw data, options
    return

  initChart = ->
    showSpinner()

    requestRealDemand().then (data) ->
      request_data = data
      hideSpinner()

      # TODO: Raise error if data is empty
      current_index = 0
      generateChartData()
      drawNextChart()
      obj.resetDrawInterval()
      return
    .fail ->
      # TODO: Manage HTTP request errors
      console.error("Oops! Something went wrong on a HTTP request.")

    return

  requestRealDemand = (callback) ->
    $.ajax
      method: 'POST'
      url: '/simulator/data/real-demand'
      dataType: 'json'
      data: Configuration.config()

  # Public methods
  obj.resetDrawInterval = ->
    return unless draw_running

    stopDrawInterval()
    interval = setInterval(drawNextChart, currentIntervalTime())

  obj.setup = ->
    # TODO: Initialize attributes, draw an empty chart or message

  obj.begin = ->
    draw_running = true
    google.charts.load 'visualization', '1', packages: ['corechart']
    google.charts.setOnLoadCallback initChart
    return

  obj

root = exports ? this
root.DemandChart = DemandChart

$ ->
  DemandChart.setup()
  return
