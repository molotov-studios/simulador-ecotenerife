# ----------------------------------
#      Configuration Management
# ----------------------------------

Configuration = do ->
  # Singleton object
  obj = {}

  # Private constants
  # --------------------------------------
  ENERGY_TOTAL_LOW  = 600
  ENERGY_TOTAL_MED  = 800
  ENERGY_TOTAL_HIGH = 1200
  ENERGY_MANAGEABLE_LOW = 300

  # Public methods
  # --------------------------------------

  obj.setup = ->
    setupInputs()
    setupEvents()
    validateConfiguration()

  obj.isPropertyEnabled = (property) ->
    $("#configuration-property-#{property}").find('input[type=checkbox]').is(':checked')

  obj.enabledProperties = ->
    $('.configuration-property')
      .map -> $(this).find('input[name=name]').val()
      .get()
      .filter (name) -> isPropertyEnabled(name)

  obj.config = ->
    output = {}

    # Setup properties
    output['properties'] = {}
    $('.configuration-property')
      .filter ->
        $(this).find('input[type=checkbox]').is(':checked')
      .each ->
        output['properties'][$(this).find('input[name=name]').val()] = {
          value: parseFloat($(this).find('input.range-value ').val())
          enabled: $(this).find('input[name=enabled]').is(':checked')
        }

    # Setup dates
    output['dates'] = {
      from: $("#date-from").data('DateTimePicker').date().toString()
      to: $("#date-to").data('DateTimePicker').date().hours(23).minutes(59).toString()
    }

    # ...

    # Return the object
    output

  # Private methods
  # --------------------------------------

  isEnabled = (property) ->
    $(this).find('input[name=name]').val()

  setupInputs = ->
    $('#date-from').datetimepicker
      format: 'DD/MM/YYYY'
      allowInputToggle: true

    $('#date-to').datetimepicker
      format: 'DD/MM/YYYY'
      allowInputToggle: true
      useCurrent: false

    $('.configuration-property').each ->
      checkProperty $(this).find('input[name=name]').val()

  setupEvents = ->
    $('#date-from-input, date-to-input').on 'keydown', ->
      false

    $('#date-from').on 'dp.change', (e) ->
      $('#date-to').data('DateTimePicker').minDate e.date
      checkConfigurationButton()

    $('#date-to').on 'dp.change', (e) ->
      $('#date-from').data('DateTimePicker').maxDate e.date
      checkConfigurationButton()

    $('.configuration-property .range-value').on 'mousemove change input', (e) ->
      Utils.clampInput $(this)
      syncEnergyRangeValue $(this).closest('.configuration-property'), $(this).val()
      updateOveralls()
      validateConfiguration()
      return true

    $('.configuration-property input[type=checkbox]').on 'change', (e) ->
      checkProperty $(this).siblings('input[name=name]').val()
      updateOveralls()
      validateConfiguration()
      return true

    $('#configuration-start-simulation').on 'click', (e) ->
      finishConfiguration()

  syncEnergyRangeValue = (parent, value) ->
    parent.find('input.range-value').val value

  checkConfigurationButton = ->
    $('#configure-button').attr 'disabled', !$('#date-from-input').val() || !$('#date-to-input').val()

  checkProperty = (name) ->
    $("#configuration-property-#{name} input.range-value").attr 'disabled', !propertyEnabled(name)

  propertyEnabled = (name) ->
    $("#configuration-property-#{name} input[type=checkbox]").is ':checked'

  updateOveralls = ->
    $('.configuration-overall').each ->
      $(this).find('.value').text totalFor($(this).find('input[name=name]').val())

  totalFor = (block_name) ->
    value = 0
    $("#configuration-block-#{block_name} .configuration-property").each ->
      name = $(this).find('input[name=name]').val()
      if propertyEnabled name
        value += parseInt $(this).find('input.range-value').val()
    value

  validateConfiguration = ->
    clearNotices()
    errors = []

    # Validate pt + non-pt
    total = totalFor('manageable') + totalFor('unmanageable')
    if total < ENERGY_TOTAL_LOW
      errors.push I18n.t('simulator.configuration.errors.total_energy_low',
        med: ENERGY_TOTAL_MED, high: ENERGY_TOTAL_HIGH)
    else if total < ENERGY_TOTAL_MED
      errors.push I18n.t('simulator.configuration.errors.total_energy_medlow',
        med: ENERGY_TOTAL_MED, high: ENERGY_TOTAL_HIGH)
    else if total > ENERGY_TOTAL_HIGH
      errors.push I18n.t('simulator.configuration.errors.total_energy_high',
        med: ENERGY_TOTAL_MED, high: ENERGY_TOTAL_HIGH)

    # Validate pt
    total = totalFor 'manageable'
    if total < ENERGY_MANAGEABLE_LOW
      errors.push I18n.t('simulator.configuration.errors.manageable_energy_low')

    # Post-validation process
    valid = (errors.length == 0)
    updateStartSimulationButton(valid)
    if valid
      showNotice I18n.t('simulator.configuration.ok.valid_configuration'), 'ok'
    else
      $(errors).each -> showNotice this.toString()

    return valid

  clearNotices = ->
    $('#notices').empty()

  showNotice = (text, class_name = 'error') ->
    $('#notices').append $('<p>', text: text, class: class_name)

  updateStartSimulationButton = (valid) ->
    button = $('#configuration-start-simulation').removeClass 'btn-primary btn-warning'
    if !!valid then button.addClass 'btn-primary' else button.addClass 'btn-warning'

  finishConfiguration = () ->
    return unless validateConfiguration() || confirm(I18n.t('buttons.are_you_sure'))

    # Hide modal, and disable controls
    $('#configuration-modal').modal 'toggle'
    $('.configuration-controls, .date-controls').find('input, button').attr 'disabled', true

    # Start simulation
    SpeedControls.enable()
    # ...

  obj

root = exports ? this
root.Configuration = Configuration

$ ->
  Configuration.setup()
  return
