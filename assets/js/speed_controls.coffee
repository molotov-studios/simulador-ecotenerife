SpeedControls = do ->
  # Singleton object
  obj = {}

  # Private fields
  time_interval = 10 * 60 * 1000 # 10 minutes
  current_speed = 1.0
  min_speed_value = 0.0001
  max_speed_value = 1
  speed_factor = 10.0

  timeFactorToHuman = (value) ->
    # If longer than 1 minute
    if value >= 60000
      [value, unit] = [value / 60000, 'minutes']
    # If longer than 1 second
    else if value >= 1000
      [value, unit] = [value / 1000, 'seconds']
    else
      [value, unit] = [value, 'milliseconds']

    value: value
    unit: I18n.t("simulator.controls.speed.units.#{unit}", count: value)

  # Private methods
  updateButtonsRestrictions = ->
    if current_speed == min_speed_value
      $('#speed-up-simulation').prop 'disabled', true
      $('#slow-down-simulation').prop 'disabled', false
    else if current_speed == max_speed_value
      $('#speed-up-simulation').prop 'disabled', false
      $('#slow-down-simulation').prop 'disabled', true
    else
      $('#speed-up-simulation').prop 'disabled', false
      $('#slow-down-simulation').prop 'disabled', false
    return

  dumpCurrentSpeed = ->
    args = timeFactorToHuman(obj.currentInterval())
    text = I18n.t('simulator.controls.speed.label', args)
    $('#speed-status').html(text)
    return

  setupEvents = ->
    $('#slow-down-simulation').click ->
      current_speed *= speed_factor if current_speed * speed_factor <= max_speed_value
      dumpCurrentSpeed()
      updateButtonsRestrictions()
      DemandChart.resetDrawInterval()
      return

    $('#speed-up-simulation').click ->
      current_speed /= speed_factor if current_speed / speed_factor >= min_speed_value
      dumpCurrentSpeed()
      updateButtonsRestrictions()
      DemandChart.resetDrawInterval()
      return

    $('#play-simulation').click ->
      $('#play-simulation').prop 'disabled', true
      DemandChart.begin()
      return

  # Public methods
  obj.enable = ->
    $('.speed-controls button').prop 'disabled', false
    updateButtonsRestrictions()
    return

  obj.setup = ->
    dumpCurrentSpeed()
    setupEvents()
    return

  obj.currentInterval = ->
    Math.round(current_speed * time_interval)

  obj

root = exports ? this
root.SpeedControls = SpeedControls

$ ->
  SpeedControls.setup()
  return
