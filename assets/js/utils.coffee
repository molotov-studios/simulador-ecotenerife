# ----------------------------------
#        General Utilities
# ----------------------------------
Utils = do ->
  # Singleton object
  obj = {}

  # Public methods
  # --------------------------------------

  obj.clampInput = (input) ->
    input.val Math.min(Math.max(input.val(), input.attr('min')), input.attr('max'))

  obj.max = (a, b) ->
    if a > b then a else b

  obj.min = (a, b) ->
    if a < b then a else b

  obj

root = exports ? this
root.Utils = Utils
